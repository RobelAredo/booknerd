package com.example.booknerd.model

import android.content.Context
import android.util.Log
import com.example.booknerd.model.Dao.BookDao
import com.example.booknerd.model.Entity.Book
import com.example.booknerd.model.remote.BookService
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.DisposableHandle
import kotlinx.coroutines.withContext
import javax.inject.Inject

class BookRepo @Inject constructor(private val service: BookService, @ApplicationContext context: Context) {

    val bookDao = BookDatabase.getInstance(context).bookDao()

    suspend fun getBooks() = withContext(Dispatchers.IO) {
        val cashedBooks: List<Book> = bookDao.getBooks()


        return@withContext cashedBooks.ifEmpty {
            val books: List<Book> = service.getBooks().map { Book(title = it.title) }
            bookDao.insert(books)
            return@ifEmpty books
        }
    }
}
