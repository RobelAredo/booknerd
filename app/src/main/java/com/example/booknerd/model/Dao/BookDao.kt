package com.example.booknerd.model.Dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.booknerd.model.Entity.Book

@Dao
interface BookDao {
    @Query("SELECT * FROM book")
    suspend fun getBooks(): List<Book>

    @Insert
    suspend fun insert(books: List<Book>)
}