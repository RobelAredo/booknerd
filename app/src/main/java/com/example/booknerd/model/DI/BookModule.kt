package com.example.booknerd.model.response

import com.example.booknerd.model.remote.BookService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
class BookModule {

    companion object {
    private const val BASE_URL = "https://the-dune-api.herokuapp.com"

    }

    @Provides
    fun providesInstance(): BookService {
        return Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build().let { it.create(BookService::class.java) }
    }
}
