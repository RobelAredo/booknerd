package com.example.booknerd.model.remote

import com.example.booknerd.model.response.Book
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface BookService {

    companion object {
        private val BASE_URL = "https://the-dune-api.herokuapp.com"

        fun providesInstance(): BookService {
            return Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build().let { it.create(BookService::class.java) }
        }
    }

    @GET("/books/{number}")
    suspend fun getBooks(@Path("number") num: Int = 100): List<Book>
}

