package com.example.booknerd;

import android.app.Application;

import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
public class BookApplication extends Application {}
