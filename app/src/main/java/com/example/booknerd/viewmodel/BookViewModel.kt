package com.example.booknerd.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.booknerd.model.BookRepo
import com.example.booknerd.model.Entity.Book
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BookViewModel @Inject constructor(private val repo: BookRepo): ViewModel() {
    private var _book = MutableLiveData<List<Book>>()
    val book: LiveData<List<Book>> get() = _book

    init {
        viewModelScope.launch {
            _book.value = repo.getBooks()
        }
    }
}
