package com.example.booknerd.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.booknerd.databinding.FragmentBookBinding
import com.example.booknerd.viewmodel.BookViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class BookFragment: Fragment() {

    private var _binding: FragmentBookBinding? = null
    private val binding get() = _binding!!
    private val bookViewModel by viewModels<BookViewModel>()
    @Inject lateinit var test: Test

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentBookBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("LOGGER", "${test.x}")
        bookViewModel.book.observe(viewLifecycleOwner) { books ->
            binding.tvBooks.text = "$books"
        }
    }
}

class Test @Inject constructor() {
    val x = 42
}