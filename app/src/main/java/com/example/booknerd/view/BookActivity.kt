package com.example.booknerd.view

import androidx.appcompat.app.AppCompatActivity
import com.example.booknerd.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BookActivity: AppCompatActivity(R.layout.activity_book)